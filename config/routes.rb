Rails.application.routes.draw do
  # ------------------
  # root
  # ------------------
  root to: "homes#top"

  # ------------------
  # homes
  # ------------------
  get "top" => "homes#top"
  get "about" => "homes#about"

  # ------------------
  # users
  # ------------------
  get "users/:id/top" => "users#top"
  get "users/index1" => "users#index1"
  get "users/index2" => "users#index2"
  get "login_form" => "users#login_form"
  post "login" => "users#login"

  # ------------------
  # posts
  # ------------------
  post "posts" => "posts#create"
  post "posts/create" => "posts#create"
  get "posts/destroy" => "posts#destroy"
  get "posts/:id/show" => "posts#show"

  # ------------------
  # replies
  # ------------------
  post "replies/:id/create" => "replies#create"
  get "reply/show" => "replies#show"

  # ------------------
  # oreshiris
  # ------------------
  get "oreshiris/create" => "oreshiris#create"
  post "oreshiris/destroy" => "oreshiris#destroy"
end
