class ChatPost < ApplicationRecord
    belongs_to :user, optional: true
    belongs_to :nice, optional: true
end
