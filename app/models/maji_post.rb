class MajiPost < ApplicationRecord
    belongs_to :user, optional: true
    belongs_to :reply, optional: true
    belongs_to :oreshiri, optional: true
end
