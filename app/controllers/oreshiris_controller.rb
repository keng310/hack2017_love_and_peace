class OreshirisController < ApplicationController
    def create
        maji_post_id = params[:id]
        Oreshiri.create!(
            user_id: 1,
            maji_content_id: maji_post_id,
            oreshiri_complete_flag: true
            )
        MajiPost.find_by(id: maji_post_id).update!(
            update_datetime: DateTime.now
            )
        redirect_to controller: 'homes', action: 'top', maji_post_id: maji_post_id
    end
    
    def destroy
    end
end
