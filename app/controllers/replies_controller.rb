class RepliesController < ApplicationController
    def create
        @maji_post = MajiPost.find_by(id: params[:id])
        @reply = Reply.new(
            user_id: 1,
            maji_post_id: @maji_post.id,
            reply_content: params[:reply_content]
            )
        @reply.save
        if @reply.save
            redirect_to("/top")
        end
    end
    
    def destroy
    end
end
