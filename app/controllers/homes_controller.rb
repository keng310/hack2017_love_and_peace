class HomesController < ApplicationController
    def top
        @maji_post_all = MajiPost.where(delete_flag: false).order("update_datetime DESC")
        @chat_post_all = ChatPost.where(delete_flag: false).order("update_datetime DESC")
    end
    
    def about
    end
end
