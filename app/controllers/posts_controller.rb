class PostsController < ApplicationController
    COMMIT_KIND_MAJI = "聞きたい！（255文字以内）"
    COMMIT_KIND_CHAT = "つぶやく（140文字以内）"
    
    def show
      @maji_post = MajiPost.find_by(id: params[:id])
      @replies = Reply.where(maji_post_id: params[:id])
    end

    
    def create
        commit_kind = params["commit"]
        post_content = params["page"]["name"]
        
        if post_content.nil?
            redirect_to controller: 'homes', action: 'top'
        else
            if commit_kind == COMMIT_KIND_MAJI
                MajiPost.create!(
                    user_id: 1,
                    maji_content: post_content,
                    update_datetime: DateTime.now
                )
            elsif commit_kind == COMMIT_KIND_CHAT
                ChatPost.create!(
                    user_id: 1,
                    chat_content: post_content,
                    update_datetime: DateTime.now
                )
            end
            redirect_to controller: 'homes', action: 'top'
        end
    end
    
    def destroy
        @target_post_id = params["id"]
        case params["type"]
        when "maji"
            MajiPost.find_by(id: @target_post_id).update!(
                delete_flag: true
                )
        when "chat"
            ChatPost.find_by(id: @target_post_id).update!(
                delete_flag: true
                )
        end
        redirect_to controller: 'homes', action: 'top'
    end
end
