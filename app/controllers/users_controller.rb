class UsersController < ApplicationController
    def top
        @maji_posts = Maji_posts.where(user_id: @current_user.id,)
        @chat_posts = Chat_posts.where(user_id: @current_user.id,)
    end
    
    def index1
        @maji_posts = Maji_posts.find_by(user_id: @current_user.id)
        @chat_posts = Chat_posts.find_by(user_id: @current_user.id)
    end
    
    def index2
        @maji_posts = Maji_posts.where(user_id: @current_user.id, resolution_flag: true)
    end
    
    def login_form
    end
    
    def login
    end
end
