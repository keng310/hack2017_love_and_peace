# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171117073410) do

  create_table "chat_posts", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "chat_content", limit: 140, null: false
    t.boolean "delete_flag", default: false
    t.datetime "update_datetime", default: "2017-11-22 12:29:22"
    t.index ["user_id"], name: "index_chat_posts_on_user_id"
  end

  create_table "maji_posts", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "maji_content", limit: 255, null: false
    t.boolean "resolution_flag", default: false
    t.boolean "delete_flag", default: false
    t.datetime "update_datetime", default: "2017-11-22 12:29:22"
    t.index ["user_id"], name: "index_maji_posts_on_user_id"
  end

  create_table "nices", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "chat_post_id", null: false
    t.boolean "nice_complete_flag", default: false
    t.index ["chat_post_id"], name: "index_nices_on_chat_post_id"
    t.index ["user_id"], name: "index_nices_on_user_id"
  end

  create_table "oreshiris", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "maji_content_id", null: false
    t.boolean "oreshiri_complete_flag", default: false
    t.index ["maji_content_id"], name: "index_oreshiris_on_maji_content_id"
    t.index ["user_id"], name: "index_oreshiris_on_user_id"
  end

  create_table "replies", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "maji_post_id", null: false
    t.string "reply_content", limit: 255, null: false
    t.index ["maji_post_id"], name: "index_replies_on_maji_post_id"
    t.index ["user_id"], name: "index_replies_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "address", limit: 255, null: false
    t.string "password", limit: 255, null: false
  end

end
