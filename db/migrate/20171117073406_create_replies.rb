class CreateReplies < ActiveRecord::Migration[5.1]
  def up
    create_table :replies do |t|
      t.references :user, :null => false
      t.references :maji_post, :null => false
      t.string :reply_content, limit: 255, :null => false
    end
  end

  def down
    drop_table :replies
  end
end
