class CreateNices < ActiveRecord::Migration[5.1]
  def up
    create_table :nices do |t|
      t.references :user, :null => false
      t.references :chat_post, :null => false
      t.boolean :nice_complete_flag, default: false
    end
  end

  def down
    drop_table :nices
  end
end
