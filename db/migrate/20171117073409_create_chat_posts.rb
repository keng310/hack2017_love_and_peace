class CreateChatPosts < ActiveRecord::Migration[5.1]
  def up
    create_table :chat_posts do |t|
      t.references :user, :null => false
      t.string :chat_content, limit: 140, :null => false
      t.boolean :delete_flag, default: false
      t.datetime :update_datetime, default: DateTime.now
    end
  end

  def down
    drop_table :chat_posts
  end
end
