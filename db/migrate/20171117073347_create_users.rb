class CreateUsers < ActiveRecord::Migration[5.1]
  def up
    create_table :users do |t|
      t.string :address, limit: 255, :null => false
      t.string :password, limit: 255, :null => false
    end
  end

  def down
    drop_table :users
  end
end
