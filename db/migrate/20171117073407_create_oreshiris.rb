class CreateOreshiris < ActiveRecord::Migration[5.1]
  def up
    create_table :oreshiris do |t|
      t.references :user, :null => false
      t.references :maji_content, :null => false
      t.boolean :oreshiri_complete_flag, default: false
    end
  end

  def down
    drop_table :oreshiris
  end
end
