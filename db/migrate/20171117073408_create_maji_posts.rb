require 'date'
class CreateMajiPosts < ActiveRecord::Migration[5.1]
  def up
    create_table :maji_posts do |t|
      t.references :user, :null => false
      t.string :maji_content, limit: 255, :null => false
      t.boolean :resolution_flag, default: false
      t.boolean :delete_flag, default: false
      t.datetime :update_datetime, default: DateTime.now
    end
  end

  def down
    drop_table :maji_posts
  end
end
